#pragma once

#include "RomanBabelfish.h"
#include <iostream>
#include <regex>

namespace roman_babelfish {

  class RomanBabelfishDialogue {
  public:
    RomanBabelfishDialogue() = delete;
    explicit RomanBabelfishDialogue(RomanBabelfish romanBabelfish);

    void printWelcomeMessage(std::ostream& ostream);
    void processInput(std::istream& istream, std::ostream& ostream);
    [[nodiscard]] bool exitRequested() const;

  private:
    struct TerminalCommand {
      std::regex regex_;
      std::string description_;
      std::function<void(const StringVector&, std::ostream&)> inputParser_;
    };

    RomanBabelfish romanBabelfish_;
    bool exitRequested_ = false;
    std::vector<TerminalCommand> terminalCommands_;
  };

}