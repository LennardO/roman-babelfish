#pragma once

#include <string>
#include <vector>

namespace roman_babelfish {

  using RomanNumeral = std::string;
  using RomanCharacter = char;
  using IntergalacticNumeral = std::string;
  using IntergalacticWord = std::string;
  using Credits = double;
  using StringVector = std::vector<std::string>;
}