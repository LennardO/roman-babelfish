#pragma once

#include "Types.h"
#include <string>
#include <memory>

namespace roman_babelfish {

  class RomanBabelfishFacility {
  public:
    virtual ~RomanBabelfishFacility() = default;

    virtual void addTranslation(const IntergalacticWord& intergalacticWord, RomanCharacter romanCharacter) = 0;
    [[nodiscard]]
    virtual unsigned int intergalacticNumeralToNumber(const IntergalacticNumeral& intergalacticNumeral) const = 0;

    virtual void addIntergalacticPrice(const IntergalacticNumeral&  intergalacticQuantity,
                                       const std::string&           productName,
                                       Credits                      totalPrice) = 0;
    [[nodiscard]] virtual Credits intergalacticPrice(const IntergalacticNumeral&  intergalacticQuantity,
                                                     const std::string&           productName) const = 0;
  };
  using RomanBabelfish = std::shared_ptr<RomanBabelfishFacility>;

}