#pragma once

#include "Types.h"
#include <stdexcept>
#include <memory>

namespace roman_babelfish {

  class IntergalacticToRomanNumeralTranslatorFacility {
  public:
    virtual ~IntergalacticToRomanNumeralTranslatorFacility() = default;
    virtual void addTranslation(const IntergalacticWord& intergalacticWord, char romanCharacter) = 0;
    [[nodiscard]] virtual RomanNumeral toRomanNumeral(const IntergalacticNumeral& intergalacticNumeral) const = 0;
  };
  using IntergalacticToRomanNumeralTranslator = std::shared_ptr<IntergalacticToRomanNumeralTranslatorFacility>;


  class IntergalacticToRomanNumeralTranslatorException : public std::invalid_argument {
  public:
    using std::invalid_argument::invalid_argument;
  };
  class UnknownRomanCharacterException : public IntergalacticToRomanNumeralTranslatorException {
  public:
    using IntergalacticToRomanNumeralTranslatorException::IntergalacticToRomanNumeralTranslatorException;
  };
  class UnknownIntergalacticWordException : public IntergalacticToRomanNumeralTranslatorException {
  public:
    using IntergalacticToRomanNumeralTranslatorException::IntergalacticToRomanNumeralTranslatorException;
  };
}
