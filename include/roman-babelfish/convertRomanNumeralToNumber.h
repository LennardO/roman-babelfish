#pragma once

#include "Types.h"
#include <stdexcept>

namespace roman_babelfish {

  class InvalidRomanNumeralException : public std::invalid_argument {
  public:
    using std::invalid_argument::invalid_argument;
  };

  unsigned int convertRomanNumeralToNumber(const RomanNumeral& romanNumeral);
}