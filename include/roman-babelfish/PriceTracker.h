#pragma once

#include "Types.h"
#include <string>
#include <memory>

namespace roman_babelfish {

  class PriceTrackerFacility {
  public:
    virtual ~PriceTrackerFacility() = default;

    virtual void addPrice(unsigned int quantity, const std::string& productName, Credits totalPrice) = 0;
    [[nodiscard]] virtual Credits price(unsigned int quantity, const std::string& productName) const = 0;
  };
  using PriceTracker = std::shared_ptr<PriceTrackerFacility>;

  class UnknownProductException : public std::invalid_argument {
  public:
    using std::invalid_argument::invalid_argument;
  };
}