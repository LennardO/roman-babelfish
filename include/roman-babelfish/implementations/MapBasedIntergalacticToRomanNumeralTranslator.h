#pragma once

#include "roman-babelfish/IntergalacticToRomanNumeralTranslator.h"
#include <map>

namespace roman_babelfish {

  class MapBasedIntergalacticToRomanNumeralTranslator : public IntergalacticToRomanNumeralTranslatorFacility {
  public:
    void addTranslation(const IntergalacticWord& intergalacticWord, RomanCharacter romanCharacter) override;
    [[nodiscard]] RomanNumeral toRomanNumeral(const IntergalacticNumeral& intergalacticNumeral) const override;

  private:
    std::map<IntergalacticWord, RomanCharacter> translations_;
  };
}