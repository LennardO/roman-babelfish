#pragma once

#include "roman-babelfish/RomanBabelfish.h"
#include "roman-babelfish/IntergalacticToRomanNumeralTranslator.h"
#include "roman-babelfish/PriceTracker.h"

namespace roman_babelfish {

  class ForwardingRomanBabelfish : public RomanBabelfishFacility {
  public:
    ForwardingRomanBabelfish() = delete;
    ForwardingRomanBabelfish(IntergalacticToRomanNumeralTranslator translator, PriceTracker priceTracker);

    void addTranslation(const IntergalacticWord& intergalacticWord, RomanCharacter romanCharacter) override;
    [[nodiscard]]
    unsigned int intergalacticNumeralToNumber(const IntergalacticNumeral& intergalacticNumeral) const override;

    void addIntergalacticPrice(const IntergalacticNumeral&  intergalacticQuantity,
                               const std::string&           productName,
                               Credits                      totalPrice) override;
    [[nodiscard]] Credits intergalacticPrice(const IntergalacticNumeral&  intergalacticQuantity,
                                             const std::string&           productName) const override;
  private:
    IntergalacticToRomanNumeralTranslator translator_;
    PriceTracker priceTracker_;
  };

}