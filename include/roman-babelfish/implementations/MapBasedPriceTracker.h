#pragma once

#include "roman-babelfish/PriceTracker.h"
#include <map>

namespace roman_babelfish {

  class MapBasedPriceTracker : public PriceTrackerFacility {
  public:
    void addPrice(unsigned int quantity, const std::string& productName, Credits totalPrice) override;
    [[nodiscard]] Credits price(unsigned int quantity, const std::string& productName) const override;

  private:
    std::map<std::string, Credits> prices_;
  };

}