#include <iostream>

#include <roman-babelfish/RomanBabelfishDialogue.h>
#include <roman-babelfish/implementations/ForwardingRomanBabelfish.h>
#include <roman-babelfish/implementations/MapBasedIntergalacticToRomanNumeralTranslator.h>
#include <roman-babelfish/implementations/MapBasedPriceTracker.h>

int main() {
  using namespace roman_babelfish;

  auto translator = std::make_shared<MapBasedIntergalacticToRomanNumeralTranslator>();
  auto priceTacker = std::make_shared<MapBasedPriceTracker>();
  auto romanBabelfish = std::make_shared<ForwardingRomanBabelfish>(translator, priceTacker);
  RomanBabelfishDialogue romanBabelfishDialogue(romanBabelfish);

  romanBabelfishDialogue.printWelcomeMessage(std::cout);
  while(!romanBabelfishDialogue.exitRequested()) {
    std::cout << "> ";
    romanBabelfishDialogue.processInput(std::cin, std::cout);
  }

  return 0;
}
