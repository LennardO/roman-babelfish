#include "roman-babelfish/RomanBabelfishDialogue.h"
#include "helperFunctions.hpp"

#include <sstream>
#include <regex>

namespace roman_babelfish {

  void RomanBabelfishDialogue::printWelcomeMessage(std::ostream& ostream) {

    ostream << "The Roman Babelfish\n"
            << "- The right tool for anyone who wants to fight capitalism by starting a trading company.\n\n"
            << "available commands:\n";
    for(auto const& terminalCommand : terminalCommands_)
      ostream << "- " << terminalCommand.description_ << '\n';
    ostream << "\nplease enter your commands:\n";
  }

  RomanBabelfishDialogue::RomanBabelfishDialogue(RomanBabelfish romanBabelfish)
  : romanBabelfish_(romanBabelfish)
  {
    terminalCommands_ = {
      {
        std::regex(R"(^\S+ is \S$)"),
        "add a translation: \"<NewIntergalacticWord> is <RomanCharacter>\"",
        [&] (const StringVector& words, std::ostream& ostream){

          romanBabelfish_->addTranslation(words[0], words[2][0]);
        }
      },
      {
        std::regex(R"(^how much is \S.* \?$)"),
        "translate intergalactic numeral: \"how much is <Intergalactic Numeral> ?\"",
        [&] (const StringVector& words, std::ostream& ostream){

          auto intergalacticNumeral = subsentence(words, 3, words.size()-2);
          auto result = romanBabelfish_->intergalacticNumeralToNumber(intergalacticNumeral);
          ostream << intergalacticNumeral << " is " << result << '\n';
        }
      },
      {
        std::regex(R"(^\S.* \S.* is ([1-9][0-9]*|0)(\.[0-9]*[1-9])? Credits$)"),
        "add a price: \"<Intergalactic Numeral> <ProductName> is <Price> Credits\"",
        [&] (const StringVector& words, std::ostream& ostream){

          auto intergalacticNumeral = subsentence(words, 0, words.size()-5);
          auto productName = words[words.size()-4];
          auto price = std::stod(words[words.size()-2]);
          romanBabelfish_->addIntergalacticPrice(intergalacticNumeral, productName, price);
        }
      },
      {
        std::regex(R"(^how many Credits is \S.* \S.* \?$)"),
        "query a price: \"how many Credits is <Intergalactic Numeral> <ProductName> ?\"",
        [&] (const StringVector& words, std::ostream& ostream){

          auto intergalacticNumeral = subsentence(words, 4, words.size()-3);
          auto productName = words[words.size()-2];
          auto price = romanBabelfish_->intergalacticPrice(intergalacticNumeral, productName);
          ostream << intergalacticNumeral << " " << productName << " is " << price << " Credits\n";
        }
      },
      {
        std::regex(R"(^exit$)"),
        "exit the application: \"exit\"",
        [&] (const StringVector& words, std::ostream& ostream){ exitRequested_ = true; }
      }
    };
  }

  void RomanBabelfishDialogue::processInput(std::istream& istream, std::ostream& ostream) {

    std::string input;
    std::getline(istream, input);

    try {

      for(auto & terminalCommand : terminalCommands_)
        if(std::regex_match(input, terminalCommand.regex_)) {
          terminalCommand.inputParser_(splitIntoWords(input), ostream);
          return;
        }

      ostream << "error: I have no idea what you are talking about.\n";
    }
    catch (const std::exception& ex) {
      ostream << "error: " << ex.what() << '\n';
    }
  }

  bool RomanBabelfishDialogue::exitRequested() const {
    return exitRequested_;
  }

}