#include "roman-babelfish/implementations/MapBasedIntergalacticToRomanNumeralTranslator.h"

#include <sstream>

namespace roman_babelfish {

  void MapBasedIntergalacticToRomanNumeralTranslator::addTranslation(const IntergalacticWord& intergalacticWord,
                                                                     const RomanCharacter     romanCharacter) {
    if(std::string("IVXLCDM").find(romanCharacter) == std::string::npos)
      throw UnknownRomanCharacterException("The provided character \'" + std::string(1, romanCharacter)
                                           + "\' is invalid. Only I, V, X, L, C, D, and M are valid.");
    translations_[intergalacticWord] = romanCharacter;
  }

  RomanNumeral MapBasedIntergalacticToRomanNumeralTranslator::toRomanNumeral(const IntergalacticNumeral& intergalacticNumeral) const {
    RomanNumeral result;

    IntergalacticWord currentWord;
    std::stringstream ss(intergalacticNumeral);
    while(ss >> currentWord)
      if(translations_.contains(currentWord))
        result += translations_.at(currentWord);
      else
        throw UnknownIntergalacticWordException("The word \"" + currentWord + "\" is not a known intergalactic word.");

    return result;
  }
}