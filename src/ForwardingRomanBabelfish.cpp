#include "roman-babelfish/implementations/ForwardingRomanBabelfish.h"
#include "roman-babelfish/convertRomanNumeralToNumber.h"

#include <utility>

namespace roman_babelfish {

  ForwardingRomanBabelfish::ForwardingRomanBabelfish(IntergalacticToRomanNumeralTranslator  translator,
                                                     PriceTracker                           priceTracker)
  : translator_(std::move(translator))
  , priceTracker_(std::move(priceTracker))
  {}

  void ForwardingRomanBabelfish::addTranslation(const IntergalacticWord&  intergalacticWord,
                                                RomanCharacter            romanCharacter) {
    translator_->addTranslation(intergalacticWord, romanCharacter);
  }

  unsigned int ForwardingRomanBabelfish::intergalacticNumeralToNumber(const IntergalacticNumeral& intergalacticNumeral) const {
    auto romanNumeral = translator_->toRomanNumeral(intergalacticNumeral);
    return convertRomanNumeralToNumber(romanNumeral);
  }

  void ForwardingRomanBabelfish::addIntergalacticPrice(const IntergalacticNumeral&  intergalacticQuantity,
                                                       const std::string&           productName,
                                                       Credits                      totalPrice) {
    auto romanQuantity = translator_->toRomanNumeral(intergalacticQuantity);
    auto quantity = convertRomanNumeralToNumber(romanQuantity);

    priceTracker_->addPrice(quantity, productName, totalPrice);
  }

  Credits ForwardingRomanBabelfish::intergalacticPrice(const IntergalacticNumeral&  intergalacticQuantity,
                                                       const std::string&           productName) const {
    auto romanQuantity = translator_->toRomanNumeral(intergalacticQuantity);
    auto quantity = convertRomanNumeralToNumber(romanQuantity);

    return priceTracker_->price(quantity, productName);
  }
}