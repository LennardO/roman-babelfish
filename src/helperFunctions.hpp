#pragma once

#include "roman-babelfish/Types.h"
#include <vector>
#include <string>
#include <sstream>

namespace roman_babelfish {

  StringVector splitIntoWords(const std::string& input) {
    std::vector<std::string> words;
    std::stringstream ss(input);
    std::string word;
    while(ss >> word)
      words.push_back(word);
    return words;
  }

  std::string subsentence(const StringVector& words, unsigned int firstWordPos, unsigned int lastWordPos) {
    std::string result = words[firstWordPos];
    for(unsigned int i=firstWordPos+1; i<=lastWordPos; i++)
      result += " " + words[i];
    return result;
  }
}