#include "roman-babelfish/implementations/MapBasedPriceTracker.h"

namespace roman_babelfish {

  void MapBasedPriceTracker::addPrice(unsigned int quantity, const std::string& productName, Credits totalPrice) {
    prices_[productName] = totalPrice / quantity;
  }

  Credits MapBasedPriceTracker::price(unsigned int quantity, const std::string& productName) const {
    if(!prices_.contains(productName))
      throw UnknownProductException("There is no known product with the name \"" + productName + "\".\n");
    return prices_.at(productName) * quantity;
  }

}