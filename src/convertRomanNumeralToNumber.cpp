#include "roman-babelfish/convertRomanNumeralToNumber.h"

#include <map>
#include <limits>
#include <regex>

namespace roman_babelfish {

  void throwIfRomanNumeralIsInvalid(const RomanNumeral& romanNumeral) {

    std::string romanNumeralRegexString = "^";
    romanNumeralRegexString += R"(M{0,3})";                    // Thousands
    romanNumeralRegexString += R"(((D?C{0,3})|(CD)?|(CM)?))";  // Hundreds
    romanNumeralRegexString += R"(((L?X{0,3})|(XL)?|(XC)?))";  // Tens
    romanNumeralRegexString += R"(((V?I{0,3})|(IV)?|(IX)?))";  // Units
    romanNumeralRegexString += "$";

    if(!std::regex_match(romanNumeral, std::regex(romanNumeralRegexString)) || romanNumeral.empty())
      throw InvalidRomanNumeralException("invalid roman numeral: " + romanNumeral);
  }

  unsigned int convertRomanNumeralToNumber(const RomanNumeral& romanNumeral) {

    throwIfRomanNumeralIsInvalid(romanNumeral);

    const std::map<RomanCharacter,unsigned int> romanCharacterValues = {
      {'I', 1},
      {'V', 5},
      {'X', 10},
      {'L', 50},
      {'C', 100},
      {'D', 500},
      {'M', 1000},
    };

    unsigned int result = 0;
    unsigned int lastValue = std::numeric_limits<unsigned int>::max();
    for(const char& romanCharacter : romanNumeral) {
      unsigned int currentValue = romanCharacterValues.at(romanCharacter);
      result += currentValue;
      if(lastValue < currentValue)
        result -= 2*lastValue;
      lastValue = currentValue;
    }

    return result;
  }
}