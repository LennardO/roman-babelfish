# The Roman Babelfish
With this tool intergalactic merchants can:
- translate intergalactic numerals
- keep track of prices for intergalactic goods

## run using docker
```bash
docker run -it lennio/roman-babelfish
```

## build it yourself
```bash
mkdir build
cd build
cmake ..
make
./build/roman-babelfish
```

## assumptions made
- An intergalactic word can consist of all kinds of characters, including +?$"@ etc.
- There may exist more than one intergalactic foreign word for a roman character.

## usage example
```bash
$ docker run -it lennio/roman-babelfish
The Roman Babelfish
- The right tool for anyone who wants to fight capitalism by starting a trading company.

available commands:
- add a translation: "<NewIntergalacticWord> is <RomanCharacter>"
- translate intergalactic numeral: "how much is <Intergalactic Numeral> ?"
- add a price: "<Intergalactic Numeral> <ProductName> is <Price> Credits"
- query a price: "how many Credits is <Intergalactic Numeral> <ProductName> ?"
- exit the application: "exit"

please enter your commands:
> tuk is I
> yak is V
> tegj is X
> kerji is L
> how much is tegj kerji yak tuk tuk ?
tegj kerji yak tuk tuk is 47
> tuk yak Spice is 1236 Credits
> how many Credits is tegj kerji tuk tuk Spice ?
tegj kerji tuk tuk Spice is 12978 Credits
> exit
```