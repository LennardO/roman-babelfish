FROM ubuntu:focal

COPY build/roman-babelfish /bin

ENTRYPOINT ["/bin/roman-babelfish"]