#include <gtest/gtest.h>
#include <roman-babelfish/implementations/MapBasedPriceTracker.h>

using namespace roman_babelfish;

TEST(MapBasedPriceTrackerTestCase, addAndQueryPrice_Works) {

  MapBasedPriceTracker priceTracker;
  priceTracker.addPrice(4, "nameOfProduct", 48);

  EXPECT_EQ(priceTracker.price(1, "nameOfProduct"), 12);
  EXPECT_EQ(priceTracker.price(5, "nameOfProduct"), 60);
}

TEST(MapBasedPriceTrackerTestCase, addAndQueryNonIntegerPrice_Works) {

  MapBasedPriceTracker priceTracker;
  priceTracker.addPrice(4, "costs0_25", 1);

  EXPECT_EQ(priceTracker.price(1, "costs0_25"), 0.25);
  EXPECT_EQ(priceTracker.price(16, "costs0_25"), 4);
}

TEST(MapBasedPriceTrackerTestCase, queryUnknownProduct_throwsException) {

  MapBasedPriceTracker priceTracker;
  EXPECT_THROW(static_cast<void>(priceTracker.price(1, "unknownName")), UnknownProductException);
}