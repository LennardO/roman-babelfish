#include "mocks/MockRomanBabelfish.h"
#include <roman-babelfish/RomanBabelfishDialogue.h>
#include <sstream>

using namespace roman_babelfish;
using ::testing::Return;
using ::testing::Throw;

class RomanBabelfishDialogueTetsCase : public ::testing::Test {
public:
  RomanBabelfishDialogueTetsCase()
  : mockRomanBabelfish_(std::make_shared<MockRomanBabelfish>())
  , romanBabelfishDialogue_(mockRomanBabelfish_)
  {}

  void processInputAndExpectOutput(const std::string& input, const std::string& expectedOutput) {
    std::stringstream inputStream(input);
    std::stringstream outputStream;
    romanBabelfishDialogue_.processInput(inputStream, outputStream);
    EXPECT_EQ(outputStream.str(), expectedOutput);
  }

protected:
  std::shared_ptr<MockRomanBabelfish> mockRomanBabelfish_;
  RomanBabelfishDialogue romanBabelfishDialogue_;
};

TEST_F(RomanBabelfishDialogueTetsCase, addTranslationInput_callsRomanBabelfish) {

  EXPECT_CALL(*mockRomanBabelfish_, addTranslation("newWord",'V'))
    .Times(1);

  processInputAndExpectOutput("newWord is V", "");
}

TEST_F(RomanBabelfishDialogueTetsCase, intergalacticNumeralToNumberInput_callsRomanBabelfish) {

  EXPECT_CALL(*mockRomanBabelfish_, intergalacticNumeralToNumber("intergalactic numeral"))
    .Times(1)
    .WillOnce(Return(17));

  processInputAndExpectOutput("how much is intergalactic numeral ?", "intergalactic numeral is 17\n");
}

TEST_F(RomanBabelfishDialogueTetsCase, addPriceInput_callsRomanBabelfish) {

  EXPECT_CALL(*mockRomanBabelfish_, addIntergalacticPrice("intergalactic numeral", "productName", 22))
    .Times(1);

  processInputAndExpectOutput("intergalactic numeral productName is 22 Credits", "");
}

TEST_F(RomanBabelfishDialogueTetsCase, addNonIntegerPrice_callsRomanBabelfish) {

  EXPECT_CALL(*mockRomanBabelfish_, addIntergalacticPrice("intergalactic numeral", "productName", 4711.42))
    .Times(1);

  processInputAndExpectOutput("intergalactic numeral productName is 4711.42 Credits", "");
}

TEST_F(RomanBabelfishDialogueTetsCase, requestPrice_callsRomanBabelfishAndReportsBack) {

  EXPECT_CALL(*mockRomanBabelfish_, intergalacticPrice("intergalactic numeral", "productName"))
    .Times(1)
    .WillOnce(Return(4711));

  processInputAndExpectOutput("how many Credits is intergalactic numeral productName ?",
                              "intergalactic numeral productName is 4711 Credits\n");
}

TEST_F(RomanBabelfishDialogueTetsCase, printNonIntegerPrice_Works) {

  EXPECT_CALL(*mockRomanBabelfish_, intergalacticPrice("intergalactic numeral", "productName"))
    .Times(1)
    .WillOnce(Return(4711.42));

  processInputAndExpectOutput("how many Credits is intergalactic numeral productName ?",
                              "intergalactic numeral productName is 4711.42 Credits\n");
}

TEST_F(RomanBabelfishDialogueTetsCase, requestingExit_Works) {

  EXPECT_FALSE(romanBabelfishDialogue_.exitRequested());
  processInputAndExpectOutput("exit", "");
  EXPECT_TRUE(romanBabelfishDialogue_.exitRequested());
}

TEST_F(RomanBabelfishDialogueTetsCase, anExceptionIsThrown_printsExceptionMessage) {

  EXPECT_CALL(*mockRomanBabelfish_, intergalacticNumeralToNumber("intergalactic numeral"))
    .Times(1)
    .WillOnce(Throw(std::runtime_error("exception message")));

  processInputAndExpectOutput("how much is intergalactic numeral ?", "error: exception message\n");
}

TEST_F(RomanBabelfishDialogueTetsCase, inputNonsense_showsAnErrorMessage) {

  processInputAndExpectOutput("Lorem ipsum dolor sit", "error: I have no idea what you are talking about.\n");
}

TEST_F(RomanBabelfishDialogueTetsCase, printWelcomeMessage_printsSomething) {

  std::stringstream outputStream;
  romanBabelfishDialogue_.printWelcomeMessage(outputStream);
  EXPECT_FALSE(outputStream.str().empty());
}