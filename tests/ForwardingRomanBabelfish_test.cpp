#include "mocks/MockIntergalacticToRomanNumeralTranslator.h"
#include "mocks/MockPriceTracker.h"
#include <roman-babelfish/implementations/ForwardingRomanBabelfish.h>

using namespace roman_babelfish;
using ::testing::Return;

class ForwardingRomanBabelfishTetCase : public ::testing::Test {
public:
  ForwardingRomanBabelfishTetCase()
  : translator_(std::make_shared<MockIntergalacticToRomanNumeralTranslator>())
  , priceTracker_(std::make_shared<MockPriceTracker>())
  , romanBabelfish_(translator_, priceTracker_)
  {}

protected:
  std::shared_ptr<MockIntergalacticToRomanNumeralTranslator> translator_;
  std::shared_ptr<MockPriceTracker> priceTracker_;
  ForwardingRomanBabelfish romanBabelfish_;
};

TEST_F(ForwardingRomanBabelfishTetCase, addTranslation_callsTranslator) {

  EXPECT_CALL(*translator_, addTranslation("newWord",'V'))
    .Times(1);

  romanBabelfish_.addTranslation("newWord", 'V');
}

TEST_F(ForwardingRomanBabelfishTetCase, intergalacticNumeralToNumber_Works) {

  EXPECT_CALL(*translator_, toRomanNumeral("intergalactic numeral"))
    .Times(1)
    .WillOnce(Return("XLII"));

  EXPECT_EQ(42, romanBabelfish_.intergalacticNumeralToNumber("intergalactic numeral"));
}

TEST_F(ForwardingRomanBabelfishTetCase, addingIntergalacticPrice_CallsTranslatorAndPriceTracker) {

  EXPECT_CALL(*translator_, toRomanNumeral("intergalactic numeral"))
    .Times(1)
    .WillOnce(Return("II"));
  EXPECT_CALL(*priceTracker_, addPrice(2, "productName", 100))
    .Times(1);

  romanBabelfish_.addIntergalacticPrice("intergalactic numeral", "productName", 100);
}

TEST_F(ForwardingRomanBabelfishTetCase, queryPrice_CallsTranslatorAndPriceTracker) {

  EXPECT_CALL(*translator_, toRomanNumeral("intergalactic numeral"))
    .Times(1)
    .WillOnce(Return("II"));
  EXPECT_CALL(*priceTracker_, price(2, "productName"))
    .Times(1)
    .WillOnce(Return(70));

  EXPECT_EQ(romanBabelfish_.intergalacticPrice("intergalactic numeral", "productName"), 70);
}