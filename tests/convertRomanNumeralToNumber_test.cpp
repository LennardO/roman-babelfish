#include <gtest/gtest.h>
#include <utility>
#include <roman-babelfish/convertRomanNumeralToNumber.h>

using namespace roman_babelfish;

class ConvertRomanNumeralAssert {
public:
  ConvertRomanNumeralAssert() = delete;
  explicit ConvertRomanNumeralAssert(std::string  romanNumeral)
  : romanNumeral_(std::move(romanNumeral))
  {}

  void isConvertedTo(unsigned int expectedNumber) const {
    EXPECT_EQ(expectedNumber, convertRomanNumeralToNumber(romanNumeral_));
  }
  
  void throwsInvalidRomanNumeralException() const {
    EXPECT_THROW(convertRomanNumeralToNumber(romanNumeral_), InvalidRomanNumeralException);
  }
private:
  std::string romanNumeral_;
};

ConvertRomanNumeralAssert checkIf(const std::string& romanNumeral) {
  ConvertRomanNumeralAssert result { romanNumeral };
  return result;
}

TEST(convertRomanNumeralToNumberTestCase, conversionOfAdditiveCharacters_Works) {

  checkIf("I").isConvertedTo(1);
  checkIf("II").isConvertedTo(2);
  checkIf("III").isConvertedTo(3);
  checkIf("X").isConvertedTo(10);
  checkIf("XX").isConvertedTo(20);
  checkIf("XXX").isConvertedTo(30);
  checkIf("C").isConvertedTo(100);
  checkIf("CC").isConvertedTo(200);
  checkIf("CCC").isConvertedTo(300);
  checkIf("M").isConvertedTo(1000);
  checkIf("MM").isConvertedTo(2000);
  checkIf("MMM").isConvertedTo(3000);

  checkIf("V").isConvertedTo(5);
  checkIf("L").isConvertedTo(50);
  checkIf("D").isConvertedTo(500);
}

TEST(convertRomanNumeralToNumberTestCase, conversionOfSubtractiveCharacters_Works) {

  checkIf("IV").isConvertedTo(4);
  checkIf("IX").isConvertedTo(9);
  checkIf("XL").isConvertedTo(40);
  checkIf("XC").isConvertedTo(90);
  checkIf("CD").isConvertedTo(400);
  checkIf("CM").isConvertedTo(900);
}

TEST(convertRomanNumeralToNumberTestCase, conversionOfCombinedNumerals_Works) {

  checkIf("MCXI").isConvertedTo(1111);
  checkIf("MMMCCCXXXIII").isConvertedTo(3333);
  checkIf("MMMDCCCLXXXVIII").isConvertedTo(3888);

  checkIf("XXXIX").isConvertedTo(39);
  checkIf("CDXLIV").isConvertedTo(444);
  checkIf("MCMXLIII").isConvertedTo(1943);
  checkIf("MMMCMXCIX").isConvertedTo(3999);
}

TEST(convertRomanNumeralToNumberTestCase, tooManyRepetitions_throwsException) {

  checkIf("IIII").throwsInvalidRomanNumeralException();
  checkIf("XXXX").throwsInvalidRomanNumeralException();
  checkIf("CCCC").throwsInvalidRomanNumeralException();
  checkIf("MMMM").throwsInvalidRomanNumeralException();

  checkIf("IIIII").throwsInvalidRomanNumeralException();
  checkIf("MMXXXXXXXXI").throwsInvalidRomanNumeralException();
  checkIf("MCCCCXI").throwsInvalidRomanNumeralException();
}

TEST(convertRomanNumeralToNumberTestCase, repeatingVLorD_throwsException) {

  checkIf("VV").throwsInvalidRomanNumeralException();
  checkIf("LL").throwsInvalidRomanNumeralException();
  checkIf("DD").throwsInvalidRomanNumeralException();

  checkIf("VVVVVVV").throwsInvalidRomanNumeralException();
  checkIf("MMDDX").throwsInvalidRomanNumeralException();
  checkIf("MMDLL").throwsInvalidRomanNumeralException();
}

TEST(convertRomanNumeralToNumberTestCase, invalidSubtractSequence_throwsException) {

  checkIf("IL").throwsInvalidRomanNumeralException();
  checkIf("IC").throwsInvalidRomanNumeralException();
  checkIf("VX").throwsInvalidRomanNumeralException();
  checkIf("XD").throwsInvalidRomanNumeralException();
  checkIf("LD").throwsInvalidRomanNumeralException();
  checkIf("DM").throwsInvalidRomanNumeralException();

  checkIf("IIX").throwsInvalidRomanNumeralException();
  checkIf("CCCMXI").throwsInvalidRomanNumeralException();
}

TEST(convertRomanNumeralToNumberTestCase, emptyInput_throwsException) {

  checkIf("").throwsInvalidRomanNumeralException();
}