#include <gtest/gtest.h>
#include <roman-babelfish/implementations/MapBasedIntergalacticToRomanNumeralTranslator.h>

using namespace roman_babelfish;

class MapBasedIntergalacticToRomanNumeralTestCase : public ::testing::Test {
public:
  MapBasedIntergalacticToRomanNumeralTestCase() {
    translator_.addTranslation("glob", 'I');
    translator_.addTranslation("prok", 'V');
    translator_.addTranslation("pish", 'X');
    translator_.addTranslation("tegj", 'L');
    translator_.addTranslation("tuk", 'C');
    translator_.addTranslation("pak", 'D');
    translator_.addTranslation("kerji", 'M');
  }

  void expectTranslation(const std::string& intergalacticNumeral, const std::string& expectedResult) const {
    EXPECT_EQ(translator_.toRomanNumeral(intergalacticNumeral), expectedResult);
  }

protected:
  MapBasedIntergalacticToRomanNumeralTranslator translator_;
};

TEST_F(MapBasedIntergalacticToRomanNumeralTestCase, translateSingleWord_Works) {

  expectTranslation("glob", "I");
  expectTranslation("prok", "V");
  expectTranslation("pish", "X");
  expectTranslation("tegj", "L");
  expectTranslation("tuk", "C");
  expectTranslation("pak", "D");
  expectTranslation("kerji", "M");
}

TEST_F(MapBasedIntergalacticToRomanNumeralTestCase, translateMultipleWords_Works) {

  expectTranslation("glob glob", "II");
  expectTranslation("glob prok", "IV");
  expectTranslation("kerji tuk kerji glob pish", "MCMIX");
}

TEST_F(MapBasedIntergalacticToRomanNumeralTestCase, addTranslationForNonexistentCharacter_throwsException) {
  EXPECT_THROW(translator_.addTranslation("word", 'P'), UnknownRomanCharacterException);
  EXPECT_THROW(translator_.addTranslation("word", 'v'), UnknownRomanCharacterException);
  EXPECT_THROW(translator_.addTranslation("word", '+'), UnknownRomanCharacterException);
}

TEST_F(MapBasedIntergalacticToRomanNumeralTestCase, translateUnknownWord_throwsException) {
  EXPECT_THROW(translator_.toRomanNumeral("glob unknown glob"), UnknownIntergalacticWordException);
  EXPECT_THROW(translator_.toRomanNumeral("GLOB"), UnknownIntergalacticWordException);
}