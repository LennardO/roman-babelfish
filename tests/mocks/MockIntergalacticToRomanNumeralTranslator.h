#pragma once

#include <roman-babelfish/IntergalacticToRomanNumeralTranslator.h>
#include <gmock/gmock.h>

class MockIntergalacticToRomanNumeralTranslator : public roman_babelfish::IntergalacticToRomanNumeralTranslatorFacility {
public:
  MOCK_METHOD(void, addTranslation, (const roman_babelfish::IntergalacticWord&  intergalacticWord,
                                     roman_babelfish::RomanCharacter            romanCharacter), (override));
  MOCK_METHOD(roman_babelfish::RomanNumeral, toRomanNumeral,
              (const roman_babelfish::IntergalacticNumeral& intergalacticNumeral), (const, override));
};