#pragma once

#include <roman-babelfish/RomanBabelfish.h>
#include <gmock/gmock.h>

class MockRomanBabelfish : public roman_babelfish::RomanBabelfishFacility {
public:
  MOCK_METHOD(void, addTranslation, (const roman_babelfish::IntergalacticWord&  intergalacticWord,
                                     roman_babelfish::RomanCharacter            romanCharacter), (override));
  MOCK_METHOD(unsigned int, intergalacticNumeralToNumber,
              (const roman_babelfish::IntergalacticNumeral& intergalacticNumeral), (const, override));
  MOCK_METHOD(void, addIntergalacticPrice, (const roman_babelfish::IntergalacticNumeral&  intergalacticQuantity,
                                           const std::string&                             productName,
                                           roman_babelfish::Credits                       totalPrice), (override));
  MOCK_METHOD(roman_babelfish::Credits, intergalacticPrice,
              (const roman_babelfish::IntergalacticNumeral& intergalacticQuantity,
               const std::string&                           productName), (const, override));
};