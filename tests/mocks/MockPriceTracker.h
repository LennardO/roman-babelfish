#pragma once

#include <roman-babelfish/PriceTracker.h>
#include <gmock/gmock.h>

class MockPriceTracker : public roman_babelfish::PriceTrackerFacility {
public:
  MOCK_METHOD(void, addPrice,
              (unsigned int quantity, const std::string& productName, roman_babelfish::Credits totalPrice), (override));
  MOCK_METHOD(roman_babelfish::Credits, price,
              (unsigned int quantity, const std::string& productName), (const, override));
};